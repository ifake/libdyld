#ifndef _MACH_O_DYLD_CLIENT_H
#define _MACH_O_DYLD_CLIENT_H

/*
 * This is kind of a non-Apple, it requires libmacho, but... this is what we do
 * inside...
 */

#include <macho-dyld/comm.h>
#include <pthread.h>
#include <sys/socket.h>

typedef struct __dyld_client *dyld_client_t;

/**
 * The client set up when the application was launched.
 * 
 * @since 0.1.0
 */
extern dyld_client_t dyld_global_client;

/**
 * Opens the global connection. Also, registers closing it to atexit.
 * 
 * @since 0.1.0
 */
int dyld_global_client_open (void);

/**
 * Creates a new client.
 * 
 * First, it creates a socket. Then, it connects to given destination.
 * 
 * After establishing a connection with dyld, a handshake packet is sent. This
 * packet contains a magic number, so it is hardly possible that if an invalid
 * argument was given, this test would be passed by it.
 * 
 * Then, the server is expected to reply with a server handshake packet. It
 * also contains a magic number, so if by accident the client handshake would
 * be sent to a wrong application, it shouldn't reply with the number. In my
 * opinion, this test is enough, as anyone might just modify dyld.
 * 
 * @param dest destination, must be of a type allowing to create sockets of
 * type <code>SOCK_STREAM</code>
 * @param dest_len length of dest, for BSD's stupid socket interface
 * @return the client, or <code>NULL</code> if something goes wrong. errno is
 * preserved
 * 
 * @since 0.1.0
 */
dyld_client_t dyld_client_open (const struct sockaddr *dest, socklen_t dest_len);

/**
 * Closes an existing client.
 * 
 * This is done by sending a goodbye packet, and again, as in
 * <code>dyld_client_open</code>, expecting another goodbye packet (here, a
 * magic number is not necessary).
 * 
 * Closing the global client is a very bad idea.
 *
 * <strong>PLEASE NOTE</strong>: In multithreaded programs, closing a client
 * whose pointer is available to any other thread is <strong>very
 * dangerous</strong>.
 * 
 * @param client client to close
 * @return <code>0</code> if successfully closed, <code>-1</code> otherwise
 * 
 * @since 0.1.0
 */
int dyld_client_close (dyld_client_t client);

#endif
