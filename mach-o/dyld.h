#ifndef _MACH_O_DYLD_H
#define _MACH_O_DYLD_H

#include <stdint.h>
#include <sys/socket.h>

/**
 * Returns the number of images currently mapped for this process.
 * 
 * In MachO, this will not list images which are not Mach-O (for example, ELF
 * libraries), because then an application might use
 * <code>_dyld_get_image_header</code>.
 * 
 * @return number of images loaded
 * 
 * @since 0.1.0
 */
uint32_t _dyld_get_image_count (void);

/**
 * Returns the pointer to the header of an image.
 * 
 * @param index index of the image
 * @return header pointer
 * 
 * @since 0.1.0
 */
const struct mach_header *_dyld_get_image_header (uint32_t index);

/**
 * Returns the name of an image.
 * 
 * @param index index of the image
 * @return image name
 * 
 * @since 0.1.0
 */
const char *_dyld_get_image_name (uint32_t index);

/**
 * Returns the amount the library was "slided" (relocated).
 * 
 * When dyld loads a library, it attempts to place it at the address it was
 * created for. But it is not always possible, conflicts happen. Then, the code
 * must be relocated. This returns the difference between the load address and
 * conceptual start address (this should be of type <code>ptrdiff_t</code>).
 * 
 * @param index number of library
 * @return offset
 * 
 * @since 0.1.0
 */
intptr_t _dyld_image_get_vmaddr_slide (uint32_t index);

#endif /* _MACH_O_DYLD_H */
