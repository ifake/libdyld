#include <config.h>

#include <dlfcn.h>
#include <mach-o/dyld.h>
#include <stdlib.h>

/**
 * Handle to a dynamic library. The dlopen call returns a pointer to one.
 * 
 * @since 0.1.0
 */
typedef struct
{
  /**
   * The dyld's index of image.
   * 
   * @since 0.1.0
   */
  uint32_t index;

  /**
   * The beginning address of image.
   * 
   * @since 0.1.0
   */
  void *start;
} __dl_handle;

void *
dlopen (const char *fname, int flags)
{
  /* TODO */
  return NULL;
}
