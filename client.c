/* MachO dyld client */

/*
 * Dyld guarantees that the environment variable DYLD_SOCKET_PATH will be a
 * valid socket.
 */

#include <config.h>

#include <mach-o/dyld_client.h>
#include <macho-dyld/comm.h>

#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

/**
 * A client for bidirectional communication with dyld.
 * 
 * @since 0.1.0
 */
struct __dyld_client
{
  /**
   * The socket FD to the connection.
   * 
   * @since 0.1.0
   */
  int socket;

  /**
   * A mutex which prevents simultaneous reads/writes.
   * 
   * Only needed in multithreaded applications, but still present, because
   * libraries might set up their threads.
   * 
   * @since 0.1.0
   */
  pthread_mutex_t mutex;
};

static void
x_close (int fd)
{
  /* Preserve errno */
  int errno_backup = errno;

  /* If someone had an error, it is not our business */
  if (fd >= 0)
    close (fd);

  /* Restore errno */
  errno = errno_backup;
}

static void
x_free (void *pointer)
{
  /* Preserve errno */
  int errno_backup = errno;

  /* Protect against free() aborting when NULL is passed */
  if (pointer != NULL)
    free (pointer);

  /* Restore errno */
  errno = errno_backup;
}

dyld_client_t dyld_global_client = NULL;

dyld_client_t
dyld_client_open (const struct sockaddr *dest, socklen_t dest_len)
{
  dyld_client_t ret = (dyld_client_t) malloc (sizeof (struct __dyld_client));

  ret->socket = socket (dest->sa_family, SOCK_STREAM, 0);

  if (ret->socket < 0)
    {
      x_free (ret);
      return NULL;
    }

  if (connect (ret->socket, dest, dest_len) == -1)
    {
      x_close (ret->socket);
      x_free (ret);
      return NULL;
    }

  return ret;
}

int
dyld_client_close (dyld_client_t client)
{
  if (close (client->socket) != 0)
    {
      x_free (client);
      return -1;
    }

  x_free (client);
  return 0;
}

int
dyld_global_client_open (void)
{
  struct sockaddr_un addr;
  addr.sun_family = AF_UNIX;
}
